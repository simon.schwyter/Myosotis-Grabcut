# Myosotis - Grabcut Algorithmus

## Usage
- Install python
- Run 'python grabcut-file-name.py image-file-name' in comand line
- Follow the descripion in the CMD


## Credits
Credits to StevenPuttemans and  alalek for Basecode from: https://github.com/opencv/opencv/blob/master/samples/python/grabcut.py