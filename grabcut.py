#!/usr/bin/env python
'''
===============================================================================
Interactive Image Segmentation using GrabCut algorithm.
This sample shows interactive image segmentation using grabcut algorithm.
USAGE:
    python grabcut.py <filename>
README FIRST:
    A window with the output will be shown. To get a better result, press 'a' to rerun grabcut 
Key 's' - To save the results
Key 'a' - To renew cut
===============================================================================
'''

# Python 2/3 compatibility
from __future__ import print_function

import numpy as np
import cv2 as cv
import sys
import time

# setting up flags
rect = (0, 0, 1, 1)
thickness = 3           # brush thickness
image_width = 0
image_height = 0
grabCut_Iterations = 1  # number of grabCut interations

# track mouse event


def runCutAlgo():
    global img, img2, mask, rect
    image_height, image_width = img.shape[:2]
    rect = (2, 2, image_height-2, image_width-2)
    cutImage()
# Runs grabcut algorithmus


def cutImage():
    print(""" For finer touchups, mark foreground and background after pressing keys 0-3
            and again press 'n' \n""")         # grabcut with rect
    bgdmodel = np.zeros((1, 65), np.float64)
    fgdmodel = np.zeros((1, 65), np.float64)
    cv.grabCut(img2, mask, rect, bgdmodel, fgdmodel,
               grabCut_Iterations, cv.GC_INIT_WITH_RECT)
			   
# Removes black background and save image
def removeBackground(src):
    tmp = cv.cvtColor(src, cv.COLOR_BGR2GRAY)
    _, alpha = cv.threshold(tmp, 0, 255, cv.THRESH_BINARY)
    b, g, r = cv.split(src)
    rgba = [b, g, r, alpha]
    dst = cv.merge(rgba, 4)
    cv.imwrite('grabcut_output'+str(time.time()) + '.png', dst)  # Save image


if __name__ == '__main__':

    # print documentation
    print(__doc__)
    # Loading images
    if len(sys.argv) == 2:
        filename = sys.argv[1]  # for drawing purposes
    else:
        print("No input image given, so loading default image, ../data/lena.jpg \n")
        print("Correct Usage: python grabcut.py <filename> \n")

    img = cv.imread(filename)
    img2 = img.copy()                               # a copy of original image
    image_height, image_width = img.shape[:2]		# get image resolution
    mask = np.zeros(img.shape[:2], dtype=np.uint8)  # mask initialized to PR_BG
    output = np.zeros(img.shape, np.uint8)          # output image to be shown

    # input and output windows
    cv.namedWindow('output')
    print(" Instructions: \n")
    print(" Draw a rectangle around the object using right mouse button \n")
    print(rect)
    runCutAlgo()
    while(1):  # draw loop

        cv.imshow('output', output)
        k = cv.waitKey(1)

        # key bindings
        if k == ord('a'):
            cutImage()
        if k == ord('s'):  # save image
            bar = np.zeros((img.shape[0], 5, 3), np.uint8)
            removeBackground(output)
            print(" Result saved as image \n")
        mask2 = np.where((mask == 1) + (mask == 3), 255, 0).astype('uint8')
        output = cv.bitwise_and(img2, img2, mask=mask2)

    cv.destroyAllWindows()
